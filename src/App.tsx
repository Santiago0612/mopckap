

import './App.css'
import LateralMenu from './LateralMenu';
import { useState } from "react"

function App() {
  const [isActive, setIsActive] = useState(false);
  const [isActiveSecond, setIsActiveSecond] = useState(false);


  const handleFirstClick = () => {
    setIsActiveSecond(false);
    setIsActive(true);

  };

  const handleSecondClick = () => {
    setIsActive(false);
    setIsActiveSecond(true);
  }
  return (
    <div className="flex h-screen bg-gray-600">
      <LateralMenu
        handlerFirstClick={handleFirstClick}
        handlerSecondClick={handleSecondClick}
        isFirstActive={isActive}
        isSecondActive={isActiveSecond}
      />

      <main className="flex-1 flex items-center justify-center">
        <div>
          {isActive ? (
            <h1 className="text-3xl font-bold">primero</h1>
          ) : isActiveSecond ? (
            <h1 className="text-3xl font-bold">segundo</h1>
          ) : null}
        </div>
      </main>
    </div>
  );
}


export default App
