import React from 'react'
import raya from "./assets/raya.svg"
import p from "./assets/p.svg"
import logo from "./assets/logo.svg"
import './App.css'
import c from "./assets/c.svg"
import salirs from "./assets/salir.svg"

interface LateralMenuProps {
    handlerFirstClick: () => void;
    handlerSecondClick: () => void;
    isFirstActive: boolean;
    isSecondActive: boolean;
}

const LateralMenu: React.FC<LateralMenuProps> = ({
    handlerFirstClick,
    handlerSecondClick,
    isFirstActive,
    isSecondActive
}) => {

  return (
<aside className="sidebar">
  <nav className="sidebar__nav">
    <div className="sidebar__logo-container">
      <img src={logo} alt="Logo" className="w-28 mt-6" />
    </div>

    <div className="sidebar__buttons">
      <button
        onClick={handlerFirstClick}
        className={`
        sidebar__button sidebar__button--first ${isFirstActive ? 'sidebar__button--active' : ''}
        `}
      >
        <img src={p} alt="Conocimiento Pragma" className="sidebar__icon" />
        <span>Conocimiento Pragma</span>
        {
            isFirstActive ? (
                <img src={raya} alt="" className="firstraya"/>
            ) : null
        }
      </button>
      <button
        onClick={handlerSecondClick}
        className={`
        sidebar__button sidebar__button--second ${isSecondActive ? 'sidebar__button--active' : ''}
        `}
      >
        <img src={c} alt="Conocimiento Pragma" className="sidebar__icon"/>
        <span >Vigilancia Conocimiento</span>
        {
            isSecondActive ? (
                <img src={raya} alt="" className="raya"/>
            ) : null
        }
      </button>
    </div>

    <footer className="sidebar__footer">
      
      <button className='sidebar__footer-button'>
      <span className="footer__text">Salir</span>
        <img src={salirs} alt="Salir" className="sidebar__icon" />
      </button>
    </footer>
  </nav>
</aside>
  )
}

export default LateralMenu